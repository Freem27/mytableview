/****************************************************************************
** Meta object code from reading C++ file 'mytableview.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mytableview.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mytableview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_myTableView_t {
    QByteArrayData data[15];
    char stringdata0[172];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_myTableView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_myTableView_t qt_meta_stringdata_myTableView = {
    {
QT_MOC_LITERAL(0, 0, 11), // "myTableView"
QT_MOC_LITERAL(1, 12, 10), // "undoFilter"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 14), // "headerMenuSlot"
QT_MOC_LITERAL(4, 39, 3), // "pos"
QT_MOC_LITERAL(5, 43, 16), // "columnFilterForm"
QT_MOC_LITERAL(6, 60, 16), // "removeHBoxLayout"
QT_MOC_LITERAL(7, 77, 12), // "newCondition"
QT_MOC_LITERAL(8, 90, 14), // "selectJoinType"
QT_MOC_LITERAL(9, 105, 11), // "setJoinType"
QT_MOC_LITERAL(10, 117, 15), // "applyFilterSlot"
QT_MOC_LITERAL(11, 133, 14), // "addGroupAction"
QT_MOC_LITERAL(12, 148, 12), // "removeLayout"
QT_MOC_LITERAL(13, 161, 8), // "QLayout*"
QT_MOC_LITERAL(14, 170, 1) // "l"

    },
    "myTableView\0undoFilter\0\0headerMenuSlot\0"
    "pos\0columnFilterForm\0removeHBoxLayout\0"
    "newCondition\0selectJoinType\0setJoinType\0"
    "applyFilterSlot\0addGroupAction\0"
    "removeLayout\0QLayout*\0l"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_myTableView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x0a /* Public */,
       3,    1,   70,    2, 0x08 /* Private */,
       5,    0,   73,    2, 0x08 /* Private */,
       6,    0,   74,    2, 0x08 /* Private */,
       7,    0,   75,    2, 0x08 /* Private */,
       8,    0,   76,    2, 0x08 /* Private */,
       9,    0,   77,    2, 0x08 /* Private */,
      10,    0,   78,    2, 0x08 /* Private */,
      11,    0,   79,    2, 0x08 /* Private */,
      12,    1,   80,    2, 0x08 /* Private */,
      12,    0,   83,    2, 0x28 /* Private | MethodCloned */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void,

       0        // eod
};

void myTableView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        myTableView *_t = static_cast<myTableView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->undoFilter(); break;
        case 1: _t->headerMenuSlot((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 2: _t->columnFilterForm(); break;
        case 3: _t->removeHBoxLayout(); break;
        case 4: _t->newCondition(); break;
        case 5: _t->selectJoinType(); break;
        case 6: _t->setJoinType(); break;
        case 7: _t->applyFilterSlot(); break;
        case 8: _t->addGroupAction(); break;
        case 9: _t->removeLayout((*reinterpret_cast< QLayout*(*)>(_a[1]))); break;
        case 10: _t->removeLayout(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QLayout* >(); break;
            }
            break;
        }
    }
}

const QMetaObject myTableView::staticMetaObject = {
    { &QTableView::staticMetaObject, qt_meta_stringdata_myTableView.data,
      qt_meta_data_myTableView,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *myTableView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *myTableView::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_myTableView.stringdata0))
        return static_cast<void*>(const_cast< myTableView*>(this));
    return QTableView::qt_metacast(_clname);
}

int myTableView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTableView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
