#include "mytableview.h"

/*
 *
 * Версия 1.0
 *
 *
 *
 *
 * */
myTableView::myTableView(QWidget *parent) :
QTableView(parent)

{
    f=new filter(0,"","",filter::JOIN_TYPE_AND,filter::CONDITION_TYPE_EQUAL,true,true);
    f->conditions.append(new filter(0,"","",filter::JOIN_TYPE_AND,filter::CONDITION_TYPE_EQUAL));
}


void myTableView::headerMenuSlot(QPoint pos)
{
    QHeaderView * header =this->horizontalHeader();
    int column=header->logicalIndexAt(pos);
    QString columnName=this->model()->headerData(column,Qt::Horizontal).toString();

    QMenu *menu =new QMenu(this);
    menu->setObjectName("headerMenu");
    menu->setProperty("column_index",column);
    menu->setProperty("column_name",columnName);

    menu->setAttribute(Qt::WA_DeleteOnClose,true);

    QAction *addColumnFilterAction=menu->addAction("Фильтр...");
    addColumnFilterAction->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/Pencil.ico"));
    connect(addColumnFilterAction,SIGNAL(triggered(bool)),this,SLOT(columnFilterForm()));

    QAction *undoFilterAction=menu->addAction("Отменить фильтр");
    undoFilterAction->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/X_close.ico"));
    connect(undoFilterAction,SIGNAL(triggered(bool)),this,SLOT(undoFilter()));
    menu->exec(QCursor::pos());
}

void myTableView::setHeaderTextColor(int col,QColor color)
{
    if(this->model()->columnCount()>=col)
        this->model()->setHeaderData(col,Qt::Horizontal,QBrush(color),Qt::TextColorRole);
}

void myTableView::setModel(QAbstractItemModel *model)
{
    QTableView::setModel(model);
    QHeaderView * header =this->horizontalHeader();
    header->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(header,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(headerMenuSlot(QPoint)));
    connect(header,SIGNAL(sectionClicked(int)),this,SLOT(sortByColumn(int)));
    header->setSortIndicator(0,Qt::AscendingOrder);
}



void myTableView::columnFilterForm()
{
    QDialog *d=new QDialog(this);
    d->setObjectName("filterForm");
    d->setWindowIcon(QIcon(":/ico/tableFilter/ico/tableFilter/Pencil.ico"));
    d->setMinimumWidth(300);
    d->setWindowFlags(d->windowFlags() &~Qt::WindowContextHelpButtonHint);
    //QString columnName=sender()->parent()->property("column_name").toString();
    //int col=sender()->parent()->property("column_index").toInt();
    d->setWindowTitle("Конструктор фильтра");
    QGridLayout *g=new QGridLayout(d);
    g->setProperty("blockRemoveGroup",true);
    g->addLayout(drawFilterGroup(f),0,0,1,2);

    QPushButton *pbOk=new QPushButton("Ok",d);
    connect(pbOk,SIGNAL(clicked(bool)),this,SLOT(applyFilterSlot()));
    pbOk->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/Checkmark_yes.ico"));
    QHBoxLayout *h=new QHBoxLayout;
    h->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding));
    h->addWidget(pbOk);
    g->addLayout(h,1,0,1,2);

    d->setStyleSheet("#filterForm {"
                     "    background: #ebebeb;"
                      "   }"

                      "   #mainFrame {"
                      "   border: 3px solid gray;"
                      "   border-radius: 40px;"
                      "   background: white;"
                      "   }"

                   " QPushButton {"
                     " color: black;"
                  //   " background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);"
                     " border-width: 1px;"
                     " border-color: #339;"
                     " border-style: solid;"
                    " border-radius: 7;"
                     " padding: 3px;"
                    " font-size: 10px;"
                    " padding-left: 5px;"
                    " padding-right: 5px;"
                   // " min-width: 50px;"
                   // " max-width: 50px;"
                    " min-height: 13px;"
                    " max-height: 13px;"
                    " }"
                     "QPushButton:hover { background-color: #ababab; }"
                 /*      ""
                     ""
                     " QLineEdit {"
                     " padding: 1px;"
                     " border-style: solid;"
                     " border: 2px solid gray;"
                     " border-radius: 8px;"
                    " }"*/
                   /*  " QComboBox {"
                     " border: 1px solid gray;"
                     " border-radius: 3px;"
                     " padding: 1px 18px 1px 3px;"
                     " min-width: 6em;"
                    " }"

                    " QComboBox::drop-down {"
                    "     subcontrol-origin: padding;"
                    "     subcontrol-position: top right;"
                    "     width: 15px;"

                    "     border-left-width: 1px;"
                    "     border-left-color: darkgray;"
                    "     border-left-style: solid;"
                    "     border-top-right-radius: 3px;"
                     "    border-bottom-right-radius: 3px;"
                    " }"
                     " QComboBox::down-arrow {"

                    " image: url(:/ico/tableFilter/ico/tableFilter/down.ico);"
                 "}"

                 "QComboBox::down-arrow:on { "
                 "    top: 1px;"
                 "    left: 1px;"
                 " }"*/
                     );
    d->exec();
    d->deleteLater();
}

QGridLayout * myTableView::drawFilterGroup(filter *filt)
{
    if(!filt)
        return 0;
    QGridLayout *g=new QGridLayout;

    if(filt->isGroup)
    {
        QHBoxLayout *h=new QHBoxLayout;
        h->setObjectName("groupHeader");
        QPushButton *pbJoinType=new QPushButton();
        pbJoinType->setObjectName("pbJoinType");
        pbJoinType->setProperty("gLayout",QVariant::fromValue(g));
        connect(pbJoinType,SIGNAL(clicked(bool)),this,SLOT(selectJoinType()));
        pbJoinType->setMaximumWidth(60);
        switch (filt->condJoinType) {
        case filter::JOIN_TYPE_AND:
            pbJoinType->setText("И");
            pbJoinType->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/and.ico"));
            break;
        case filter::JOIN_TYPE_OR:
            pbJoinType->setText("ИЛИ");
            pbJoinType->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/or.ico"));
            break;
        default:
            break;
        }

        QPushButton *pbAddCondition=new QPushButton;
        pbAddCondition->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/Plus.ico"));

        connect(pbAddCondition,SIGNAL(clicked(bool)),this,SLOT(newCondition()));

        pbAddCondition->setProperty("gLayout",QVariant::fromValue(g));
        pbAddCondition->setMaximumWidth(25);

        QPushButton *pbDelGroup=new QPushButton;

        pbDelGroup->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/X_close.ico"));
        if(!filt->isParetn)
        {
            pbDelGroup->setProperty("gLayout",QVariant::fromValue(g));
            connect(pbDelGroup,SIGNAL(clicked(bool)),this,SLOT(removeLayout()));


            pbDelGroup->setMaximumWidth(25);
            h->addWidget(pbJoinType);
            h->addWidget(pbAddCondition);
            h->addSpacerItem(new QSpacerItem(0,0, QSizePolicy::Expanding));
            pbAddCondition->click();


            h->addWidget(pbDelGroup);
            g->setObjectName("childConditionGrid");
        }else
        {
            h->addWidget(pbJoinType);
            h->addWidget(pbAddCondition);
            h->addSpacerItem(new QSpacerItem(0,0, QSizePolicy::Expanding));
            g->setObjectName("parentConditionGrid");
        }


        g->addLayout(h,0,0,1,2);
    }

    for(int c=0;c<filt->conditions.count();c++)
    {

        if(filt->conditions.at(c)->isGroup)
            g->addLayout(drawFilterGroup(filt->conditions.at(c)),g->rowCount()+1,1,1,1);
        else
        {
            int row=g->rowCount();
            //g->addWidget(l,row,0);
            g->addLayout(newConditionRow(filt->conditions.at(c)->colName,filt->conditions.at(c)->conditionType,filt->conditions.at(c)->value),row,1);
        }

    }
    return g;

}

void myTableView::removeLayout(QLayout *l)
{
    if(!l)
        l=sender()->property("gLayout").value<QGridLayout*>()->layout();


    for(int i=0;i<l->count();i++)
    {
        if(l->itemAt(i)->widget())
            l->itemAt(i)->widget()->deleteLater();
        if(l->itemAt(i)->layout())
            removeLayout(l->itemAt(i)->layout());
    }
    l->deleteLater();
}

void myTableView::applyFilterSlot()
{
    //сохранить фильтр
    QGridLayout *g =sender()->parent()->findChild<QGridLayout *>()->findChild<QGridLayout*>();
    f->clear();
    if(g->objectName()=="parentConditionGrid")
        f=getFilter(g);
    f->isParetn=true;
    //применить фильтр
    for(int row=0;row<model()->rowCount();row++)
    {
        QStringList rec;
        for(int col=0;col<model()->columnCount();col++)
            rec.append(model()->data(model()->index(row,col)).toString());
        f->rowCorrespondsCondition(rec,f) ? this->showRow(row) : this->hideRow(row);
    }
    //убрать подствету со столбцов
    for(int col=0;col<model()->columnCount();col++)
        setHeaderTextColor(col);
    //подстветить задействованные столбцы
    QList<int> columns=filter::filterColumns(f);
    for(int col=0;col<columns.count();col++)
        setHeaderTextColor(columns.at(col),QColor(255,0,0));

    qobject_cast<QDialog*>(sender()->parent())->close();
}


filter* myTableView::getFilter(QGridLayout *g)
{
    filter *result=new filter();
    result->isGroup=true;
    for(int i=0;i<g->count();i++)
    {
        QLayout *l=g->itemAt(i)->layout();
        if(l)
        {
            if(l->objectName()=="groupHeader")
            {
                for(int j=0;j<l->count();j++)
                {
                    QPushButton *pb=qobject_cast<QPushButton*>(l->itemAt(j)->widget());
                    if(pb and pb->objectName()=="pbJoinType")
                        pb->text()=="И" ? result->condJoinType=filter::JOIN_TYPE_AND : result->condJoinType=filter::JOIN_TYPE_OR;
                }
            }
            if(l->objectName()=="condition")
            {
                filter *cond=new filter();
                cond->isGroup=false;
                for(int j=0;j<l->count();j++)
                {
                    if(l->itemAt(j)->widget()->objectName()=="cbColumnsList")
                    {
                        QComboBox *cbColumnsList=qobject_cast<QComboBox*>(l->itemAt(j)->widget());
                        cond->col=cbColumnsList->currentData(COLUMN_INDEX).toInt();
                        cond->colName=cbColumnsList->currentText();
                    }
                    if(l->itemAt(j)->widget()->objectName()=="cbCondList")
                    {
                        QComboBox *cbCondList=qobject_cast<QComboBox*>(l->itemAt(j)->widget());
                        cond->conditionType=cbCondList->currentData(CONDITION_TYPE).toInt();
                    }
                    if(l->itemAt(j)->widget()->objectName()=="leValue")
                    {
                        cond->value=qobject_cast<QLineEdit*>(l->itemAt(j)->widget())->text();
                    }

                }
                result->conditions.append(cond);

            }
            if(l->objectName()=="childConditionGrid")
            {
                result->conditions.append(getFilter(qobject_cast<QGridLayout *>(l)));
            }
        }
    }
    return result;
}

QComboBox *myTableView::columnsList(QString colName)
{
    QComboBox *cb=new QComboBox;
    cb->setObjectName("cbColumnsList");
    for(int col=0;col<model()->columnCount();col++)
    {
        if(!isColumnHidden(col))
        {
            cb->addItem(model()->headerData(col,Qt::Horizontal).toString());
            cb->setItemData(cb->count()-1,col,COLUMN_INDEX);
        }
    }
    cb->setCurrentText(colName);
    return cb;
}

void myTableView::newCondition()
{
    QGridLayout *g=sender()->property("gLayout").value<QGridLayout*>();
    g->addLayout(newConditionRow(),g->rowCount(),1);
}

void myTableView::selectJoinType()
{
    QPushButton *pbJoinType=qobject_cast<QPushButton*>(sender());

    QMenu *menu=new QMenu(this);
    menu->setProperty("pbJoinType",QVariant::fromValue(pbJoinType));
    menu->setAttribute(Qt::WA_DeleteOnClose,true);

    QAction *andAction=menu->addAction("И");
    andAction->setObjectName("andAction");
    andAction->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/and.ico"));
    connect(andAction,SIGNAL(triggered(bool)),this,SLOT(setJoinType()));

    QAction *orAction=menu->addAction("ИЛИ");
    orAction->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/or.ico"));
    orAction->setObjectName("orAction");
    connect(orAction,SIGNAL(triggered(bool)),this,SLOT(setJoinType()));


    QAction *addGroupAction=menu->addAction("Добавить группу");
    addGroupAction->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/Plus.ico"));
    connect(addGroupAction,SIGNAL(triggered(bool)),this,SLOT(addGroupAction()));

    //QAction *clearAction=menu->addAction("Очистить");

    menu->exec(QCursor::pos());
}

void myTableView::undoFilter()
{
    for(int row=0;row<model()->rowCount();row++)
        showRow(row);
    for(int col=0;col<model()->columnCount();col++)
        setHeaderTextColor(col);
}

void myTableView::setJoinType()
{
    QPushButton *pbJoinType=sender()->parent()->property("pbJoinType").value<QPushButton*>();
    if(sender()->objectName()=="orAction"){
        pbJoinType->setText("ИЛИ");
        pbJoinType->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/or.ico"));
    }
    if(sender()->objectName()=="andAction")
    {
        pbJoinType->setText("И");
        pbJoinType->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/and.ico"));
    }
}

void myTableView::addGroupAction()
{
    QPushButton *pbJoinType=sender()->parent()->property("pbJoinType").value<QPushButton*>();
    QGridLayout *g=pbJoinType->property("gLayout").value<QGridLayout*>();


    g->addLayout(drawFilterGroup(new filter(0,"","",filter::JOIN_TYPE_AND,filter::CONDITION_TYPE_EQUAL,true)),g->rowCount(),1);
   // g->addWidget(l,row,0);
}

QHBoxLayout* myTableView::newConditionRow(QString selectedColumn,int selectedCondition,QString value)
{
    QHBoxLayout *h=new QHBoxLayout;
    h->setObjectName("condition");
    h->addWidget(columnsList(selectedColumn));
    h->addWidget(condList(selectedCondition));
    QLineEdit *leValue=new QLineEdit(value);
    leValue->setPlaceholderText("Введите значение");
    leValue->setObjectName("leValue");
    h->addWidget(leValue);
    QPushButton *pbDel=new QPushButton;
    pbDel->setIcon(QIcon(":/ico/tableFilter/ico/tableFilter/X_close.ico"));
    connect(pbDel,SIGNAL(clicked(bool)),this,SLOT(removeHBoxLayout()));
    pbDel->setMaximumWidth(25);
    pbDel->setProperty("layout",QVariant::fromValue(h));
    h->addWidget(pbDel);
    return h;
}

void myTableView::removeHBoxLayout()
{
    QHBoxLayout* h=sender()->property("layout").value<QHBoxLayout*>();
    for(int i=0;i<h->count();i++)
        h->itemAt(i)->widget()->deleteLater();
    h->deleteLater();
}

QComboBox *myTableView::condList(int selected)
{
    QComboBox *cb=new QComboBox;
    cb->setObjectName("cbCondList");
    cb->addItem("=");
    cb->setItemData(cb->count()-1,filter::CONDITION_TYPE_EQUAL,CONDITION_TYPE);
    cb->addItem(">");
    cb->setItemData(cb->count()-1,filter::CONDITION_TYPE_MORE,CONDITION_TYPE);
    cb->addItem("<");
    cb->setItemData(cb->count()-1,filter::CONDITION_TYPE_LESS,CONDITION_TYPE);
    cb->addItem(">=");
    cb->setItemData(cb->count()-1,filter::CONDITION_TYPE_MORE_OR_EQUAL,CONDITION_TYPE);
    cb->addItem("<=");
    cb->setItemData(cb->count()-1,filter::CONDITION_TYPE_LESS_OR_EQUAL,CONDITION_TYPE);
    cb->addItem("><");
    cb->setItemData(cb->count()-1,filter::CONDITION_TYPE_NOT_EQUAL,CONDITION_TYPE);
    cb->addItem("содержит");
    cb->setItemData(cb->count()-1,filter::CONDITION_TYPE_CONTAIN,CONDITION_TYPE);
    cb->addItem("не содержит");
    cb->setItemData(cb->count()-1,filter::CONDITION_TYPE_NOT_CONTAIN,CONDITION_TYPE);
    for(int i=0;i<cb->count();i++)
        if(cb->itemData(i,CONDITION_TYPE).toInt()==selected)
        {
            cb->setCurrentIndex(i);
            break;
        }
    return cb;
}
