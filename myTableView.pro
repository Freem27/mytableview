#-------------------------------------------------
#
# Project created by QtCreator 2016-06-01T16:13:27
#
#-------------------------------------------------

QT       += widgets

TARGET = myTableView
TEMPLATE = lib
DESTDIR = dist
#DEFINES += MYTABLEVIEW_LIBRARY

SOURCES += mytableview.cpp

HEADERS += mytableview.h

RESOURCES += filterres.qrc

VERSION = 1.0.0

TARGET = $$qtLibraryTarget(myTableView)

CONFIG += build_all
unix {
    target.path = /usr/lib
    INSTALLS += target
}
