#ifndef MYTABLEVIEW_H
#define MYTABLEVIEW_H

#include <QObject>
#include "QTableView"
#include "QMenu"
#include "QGridLayout"
#include "QPushButton"
#include "QComboBox"
#include "QLineEdit"
#include "QStandardItemModel"
#include "QStringListModel"
#include "QDebug"
#include "QHeaderView"
#include "QDialog"
#include "QLabel"

struct filter{
    const static int JOIN_TYPE_AND=1;
    const static int JOIN_TYPE_OR=0;

    const static int CONDITION_TYPE_EQUAL=0;
    const static int CONDITION_TYPE_MORE=1;
    const static int CONDITION_TYPE_LESS=2;
    const static int CONDITION_TYPE_MORE_OR_EQUAL=3;
    const static int CONDITION_TYPE_LESS_OR_EQUAL=4;
    const static int CONDITION_TYPE_NOT_EQUAL=5;
    const static int CONDITION_TYPE_CONTAIN=6;
    const static int CONDITION_TYPE_NOT_CONTAIN=7;

    filter(int colIndex=0,QString colName="", QString value="", int condJoinType=filter::JOIN_TYPE_AND,int conditionType=filter::CONDITION_TYPE_EQUAL,bool isGroup=false,bool isParent=false)
    {
        this->condJoinType=condJoinType;
        this->conditionType=conditionType;
        this->col=colIndex;
        this->colName=colName;
        this->value=value;
        this->isGroup=isGroup;
        this->isParetn=isParent;
    }

    void clear()
    {
        isGroup=false;
        conditionType=-1;
        col=-1;
        colName.clear();
        value.clear();
                condJoinType=-1;
        for(int i=0;i<conditions.count();i++)
            conditions.at(i)->clear();
        conditions.clear();
    }
    bool isParetn;

    static bool rowCorrespondsCondition(QStringList row,filter *f)
    {
        bool corresponds=true;
        if(f->isGroup)
        {
            switch (f->condJoinType)
            {
            case filter::JOIN_TYPE_AND:
                for(int c=0;c<f->conditions.count();c++)
                    if(!rowCorrespondsCondition(row,f->conditions.at(c)))
                    {
                        corresponds=false;
                        break;
                    }
                break;
            case filter::JOIN_TYPE_OR:
            {
                //если хотябы один подходит, то ок
                bool tempCorresponds=false;
                for(int c=0;c<f->conditions.count();c++)
                    if(rowCorrespondsCondition(row,f->conditions.at(c)))
                    {
                        tempCorresponds=true;
                        break;
                    }
                corresponds=tempCorresponds;
                break;
            }
            default:
                break;
            }
        }else
        {

            bool isConvert=false;
            int rowInt=QString(row.at(f->col)).toInt(&isConvert);
            int valueInt;
            if(isConvert)
                valueInt=f->value.toInt(&isConvert);
            switch (f->conditionType) {
            case filter::CONDITION_TYPE_EQUAL:
                if(row.at(f->col)!=f->value)
                    corresponds=false;
                break;
            case filter::CONDITION_TYPE_NOT_EQUAL:
                if(row.at(f->col)==f->value)
                    corresponds=false;
                break;
            case filter::CONDITION_TYPE_CONTAIN:
                if(!QString(row.at(f->col)).toUpper().contains(f->value.toUpper()))
                    corresponds=false;
                break;
            case filter::CONDITION_TYPE_NOT_CONTAIN:
                if(QString(row.at(f->col)).toUpper().contains(f->value.toUpper()))
                    corresponds=false;
                break;
            case filter::CONDITION_TYPE_MORE:
                if(rowInt<=valueInt or !isConvert)
                    corresponds=false;
                break;
            case filter::CONDITION_TYPE_LESS:
                if(rowInt>=valueInt or !isConvert)
                    corresponds=false;
                break;

            case filter::CONDITION_TYPE_MORE_OR_EQUAL:
                if(!(rowInt>=valueInt) or !isConvert)
                    corresponds=false;
                break;
            case filter::CONDITION_TYPE_LESS_OR_EQUAL:
                if(!(rowInt<=valueInt) or !isConvert)
                    corresponds=false;
                break;
            default:
                break;
            }
        }

        return corresponds;
    }

    const static QList<int> filterColumns(filter *f)
    {
        QList<int> result;
        if(f->isGroup)
        {
            for(int i=0;i<f->conditions.count();i++)
                result.append(filterColumns(f->conditions.at(i)));
        }else
        {
            result.append(f->col);
        }
        //удаление дубликатов
        for(int i=0;i<result.count();i++)
        {
            int value=result.at(i);
            for(int j=i+1;j<result.count();j++)
                if(result.at(j)==value)
                {
                    result.removeAt(j);
                    j--;
                }

        }
        return result;
    }

    bool isGroup;
    int conditionType; // =; >=; <=; ><;
    int col;
    QString colName;
    QString value;
    int condJoinType;// OR; AND
    QList<filter*> conditions;
};

class myTableView : public QTableView
{
    Q_OBJECT

public:
    const static int COLUMN_INDEX=54;
    const static int CONDITION_TYPE=55;
    explicit myTableView(QWidget *parent = 0);


    void setModel(QAbstractItemModel *model);




public slots:
    void undoFilter();
private slots:
    void headerMenuSlot(QPoint pos);
    void columnFilterForm();
    void removeHBoxLayout();
    void newCondition();
    void selectJoinType();
    void setJoinType();
    void applyFilterSlot();
    void addGroupAction();
    void removeLayout(QLayout *l=0);
private:
    filter *f;
    void setHeaderTextColor(int col, QColor color=QColor(0,0,0));
    QGridLayout *drawFilterGroup(filter *filt);
    QComboBox *columnsList(QString colName="");
    QComboBox *condList(int selected=-1);
    QHBoxLayout *newConditionRow(QString selectedColumn="", int selectedCondition=-1, QString value="");
    filter *getFilter(QGridLayout *g);
    void applyFilterToRow(int row);
};

#endif // MYTABLEVIEW_H
